# Commit To Repos During CI

Demonstrates committing to the repo the CI runs in as well as cloning and committing to another repo the runner has access to.

This technique can also be used to update the project wiki with automated releases notes, etc.

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/gitlab-ci-yml-tips-tricks-and-hacks/commit-to-repos-during-ci/commit-to-repos-during-ci)

## Use Cases
* Across Pipeline Version tracking persistence in files
* Automated release notes and/or CHANGELOG.md
* Automated Project Wiki Update (it's a standalone repo of markdown files)
